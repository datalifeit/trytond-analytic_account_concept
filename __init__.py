# The COPYRIGHT file at the top level of this repository contains
# the full copyright notices and license terms.
from trytond.pool import Pool
from .analytic import AnalyticConcept, AnalyticConceptLine


def register():
    Pool.register(
        AnalyticConcept,
        AnalyticConceptLine,
        module='analytic_account_concept', type_='model')
